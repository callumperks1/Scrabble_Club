import Database.Database;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.*;
import javafx.application.*;
import javafx.stage.*;

import UI.*;

public class UI extends Application{

    @Override
    public void start(Stage primaryStage){
        primaryStage.setTitle("Scrabble Club");
        Pane root = new Pane();
        Database database = new Database("jdbc:sqlite:database/database.db");
        TextField textField = new TextField();
        HBox hbox = new HBox();

        //create and position submit button
        Button submit = new Button();
        submit.setText("Submit name");
        submit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(!root.getChildren().isEmpty()){
                    root.getChildren().clear();
                }
                //get member object and create ui from it then add to screen
                Member member = database.viewMember(textField.getText());
                MemberUI ui = new MemberUI(member);
                root.getChildren().add(hbox);
                root.getChildren().add(ui.layout);
            }
        });

        //create and position leaderboard button
        Button leaderboardButton = new Button();
        leaderboardButton.setText("Leaderboard");
        leaderboardButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(!root.getChildren().isEmpty()){
                    root.getChildren().clear();
                }
                //create and position leaderboard
                Leaderboard leaderboard = new Leaderboard(database.viewLeaderboard());
                root.getChildren().add(hbox);
                root.getChildren().add(leaderboard.grid);
            }
        });

        //add buttons to screen
        hbox.getChildren().add(textField);
        hbox.getChildren().add(submit);
        hbox.getChildren().add(leaderboardButton);
        root.getChildren().add(hbox);




        primaryStage.setScene(new Scene(root, 480, 480));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
