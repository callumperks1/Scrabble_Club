package UI;

import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import java.util.List;

public class Leaderboard {
    public GridPane grid = new GridPane();

    public Leaderboard(List<LeaderboardMember> members){
        for(int i = 0; i < members.size(); i++){
                // Show name
                Label name = new Label();
                name.setText(members.get(i).getName());
                grid.add(name, 0, i);

                // Show average score
                Label score = new Label();
                score.setText("" + members.get(i).getAverageScore());
                grid.add(score, 1, i);

                grid.setLayoutX(25);
                grid.setLayoutY(25);
        }
    }
}
