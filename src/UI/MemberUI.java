package UI;

import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;


enum MemberFields {
    name,
    wins,
    losses,
    averageScore,
    highScore,
    highScoreWhen,
    highScoreWhere,
    highScoreWho,
    matchesPlayed
}

public class MemberUI {
    public GridPane layout = new GridPane();
    Label[] titleLabels = new Label[MemberFields.values().length];
    Label[] infoLabels = new Label[MemberFields.values().length];


    public MemberUI(Member member) {
        layout.setLayoutX(25);
        layout.setLayoutY(25);

        //loop through each field
        for (int i = 0; i < MemberFields.values().length; i++) {
            titleLabels[i] = new Label();
            titleLabels[i].setText(MemberFields.values()[i].name());
            layout.add(titleLabels[i], 0, i);

            // add data to screen
            Label tempLabel = new Label();
            switch (i){
                case 0: {
                    tempLabel.setText("" + member.getName());
                    layout.add(tempLabel, 1, 0);
                    break;
                }
                case 1:{
                    tempLabel.setText("" + member.getWins());
                    layout.add(tempLabel, 1, 1);
                    break;
                }
                case 2:{
                    tempLabel.setText("" + member.getLosses());
                    layout.add(tempLabel, 1, 2);
                    break;
                }
                case 3:{
                    tempLabel.setText("" + member.getAverageScore());
                    layout.add(tempLabel, 1, 3);
                    break;
                }
                case 4:{
                    tempLabel.setText("" + member.getHighScore());
                    layout.add(tempLabel, 1, 4);
                    break;
                }
                case 5:{
                    tempLabel.setText("" + member.getHighScoreWho());
                    layout.add(tempLabel, 1, 5);
                    break;
                }
                case 6:{
                    tempLabel.setText("" + member.getHighScoreWhere());
                    layout.add(tempLabel, 1, 6);
                    break;
                }
                case 7:{
                    tempLabel.setText("" + member.getHighScoreWhen());
                    layout.add(tempLabel, 1, 7);
                    break;
                }
                case 8:{
                    tempLabel.setText("" + member.getMatchesPlayed());
                    layout.add(tempLabel, 1, 8);
                    break;
                }
        }
    }
    }

}
