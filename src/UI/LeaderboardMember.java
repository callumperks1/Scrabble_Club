package UI;

public class LeaderboardMember {
    private String name;
    private double averageScore;

    public LeaderboardMember(String name, double averageScore) {
        this.name = name;
        this.averageScore = averageScore;
    }

    public String getName() {
        return name;
    }

    public double getAverageScore() {
        return averageScore;
    }
}