package UI;

import java.util.ArrayList;
import java.util.List;

public class Member {
    private String name;
    private int wins;
    private int losses;
    private double averageScore;
    private int highScore;
    private String highScoreWhere;
    private String highScoreWhen;
    private String highScoreWho;
    private int matchesPlayed;

    public Member(String name, int wins, int losses, double averageScore, int highScore, String highScoreWhere, String highScoreWhen, String highScoreWho, int matchesPlayed) {
        this.name = name;
        this.wins = wins;
        this.losses = losses;
        this.averageScore = averageScore;
        this.highScore = highScore;
        this.highScoreWhere = highScoreWhere;
        this.highScoreWhen = highScoreWhen;
        this.highScoreWho = highScoreWho;
        this.matchesPlayed = matchesPlayed;
    }

    public String getName() {
        return name;
    }

    public int getWins() {
        return wins;
    }

    public int getLosses() {
        return losses;
    }

    public double getAverageScore() {
        return averageScore;
    }

    public int getHighScore() {
        return highScore;
    }

    public String getHighScoreWhere() {
        return highScoreWhere;
    }

    public String getHighScoreWhen() {
        return highScoreWhen;
    }

    public String getHighScoreWho() {
        return highScoreWho;
    }

    public int getMatchesPlayed(){
        return matchesPlayed;
    }

}
