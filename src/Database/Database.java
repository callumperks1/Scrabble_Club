package Database;

import UI.LeaderboardMember;
import UI.Member;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Database {
    Connection connection = null;

    public Database(String url) {
        try {
            //create connection and log
            connection = DriverManager.getConnection(url);
            System.out.println("Connection established");
        } catch (SQLException e) {
            System.out.println("JDBC Error: Connection could not be made!");
        }
    }


    public Member viewMember(String name) {
        //sql query to retrieve a row
        String sql = "SELECT * FROM members WHERE NAME = ?;";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, name);
            ResultSet results = statement.executeQuery();
            //convert the data to a Member object
            while (results.next()) {
                return new Member(results.getString("name"), results.getInt("wins"), results.getInt("losses"), results.getDouble("average"), results.getInt("high_score"),
                        results.getString("high_score_where"), results.getString("high_score_when"), results.getString("high_score_who"), results.getInt("matches_played"));

            }
        } catch (SQLException e) {
        }
        return null;
    }


    public List<LeaderboardMember> viewLeaderboard() {
        List<LeaderboardMember> members = new ArrayList<>();

        //query to generate leaderboard data
        String sql = " SELECT name, average FROM members WHERE matches_played > 9 ORDER BY average DESC;";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet results = statement.executeQuery();
            System.out.println("");

            // loop throught he data
            while (results.next()) {
                members.add(new LeaderboardMember(results.getString("name"),results.getDouble("average")));
            }
            return members;
        } catch (SQLException e) {
            System.out.println("JDBC error");
        }
        return null;
    }
}
